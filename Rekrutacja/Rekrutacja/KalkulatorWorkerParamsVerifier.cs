﻿using Rekrutacja.Workers.Template;
using Soneta.Business;
using Soneta.Tools;
using Soneta.Types;

namespace Rekrutacja
{
    public class KalkulatorWorkerParamsVerifier : Verifier
    {
        private const string ERROR_MESSAGE = "Nie można dzielić przez zero";

        private readonly KalkulatorWorkerParams _kalkulatorWorkerParams;

        public KalkulatorWorkerParamsVerifier(KalkulatorWorkerParams kalkulatorWorkerParams)
        {
            _kalkulatorWorkerParams = kalkulatorWorkerParams;
        }

        protected override bool IsValid()
        {
            var param = Source as KalkulatorWorkerParams;

            if (param?.Operacja == Operacja.Dzielenie && param.LiczbaB == 0)
            {
                return false;
            }

            return true;
        }

        protected override bool IsAccept(object data, string property, VerifierAction action)
        {
            return data == _kalkulatorWorkerParams && property == nameof(KalkulatorWorkerParams.LiczbaB);
        }

        public override string Description
        {
            get { return ERROR_MESSAGE.Translate(); }

        }

        public override object Source
        {
            get { return _kalkulatorWorkerParams; }
        }
    }
}