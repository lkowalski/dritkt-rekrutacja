﻿using Rekrutacja.Workers;
using Soneta.Business;
using Soneta.Kadry;

[assembly: Worker(typeof(KalkulatorWorker), typeof(Pracownicy))]

namespace Rekrutacja.Workers
{
    public class KalkulatorWorker
    {
        private const string ACTION_NAME = "Kalkulator";
        private const string ACTION_DESCRIPTION = "Prosty kalkulator";
        private const string FEATURE_OPERATION_DATE = "DataObliczen";
        private const string FEATURE_RESULT = "Wynik";

        [Context]
        public KalkulatorWorkerParams Params { get; set; }

        [Context]
        public Pracownik[] Pracownicy { get; set; }

        [Action(ACTION_NAME,
            Description = ACTION_DESCRIPTION,
            Priority = 10,
            Mode = ActionMode.SingleSession,
            Icon = ActionIcon.Tools,
            Target = ActionTarget.ToolbarWithText)]
        public void WykonajAkcje()
        {
            DebuggerSession.MarkLineAsBreakPoint();

            double wynik = Kalkulator.Oblicz(Params.Operacja, Params.LiczbaA, Params.LiczbaB);

            using (ITransaction transaction = Params.Session.Logout(true))
            {
                foreach (Pracownik pracownik in Pracownicy)
                {
                    pracownik.Features[FEATURE_OPERATION_DATE] = Params.DataObliczen;
                    pracownik.Features[FEATURE_RESULT] = wynik;
                }

                transaction.CommitUI();
            }
        }
    }
}