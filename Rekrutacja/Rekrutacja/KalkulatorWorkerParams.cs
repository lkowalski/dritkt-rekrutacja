﻿using Rekrutacja.Workers.Template;
using Soneta.Business;
using Soneta.Tools;
using Soneta.Types;

namespace Rekrutacja
{
    public class KalkulatorWorkerParams : ContextBase
    {
        [Priority(10)]
        [Caption("A")]
        public double LiczbaA { get; set; }

        [Priority(20)]
        [Caption("B")]
        public double LiczbaB { get; set; }

        [Priority(30)]
        [Caption("Operacja")]
        public Operacja Operacja { get; set; }

        [Required]
        [Priority(40)]
        [Caption("Data obliczeń")]
        public Date DataObliczen { get; set; }

        public KalkulatorWorkerParams(Context context) : base(context)
        {
            DataObliczen = Date.Today;

            Session.Verifiers.Add(new KalkulatorWorkerParamsVerifier(this));
        }
    }
}
