﻿using Soneta.Types;

namespace Rekrutacja.Workers.Template
{
    public enum Operacja
    {
        [Caption("+")]
        Dodawanie,

        [Caption("-")]
        Odejmowanie,

        [Caption("*")]
        Mnozenie,

        [Caption("/")]
        Dzielenie,
    }
}