﻿using System;
using Rekrutacja.Workers.Template;
using Soneta.Tools;

namespace Rekrutacja
{
    public static class Kalkulator
    {
        private static string INVALID_OPERATION_ERROR_MESSAGE = "Nieprawidłowy typ operacji";

        public static double Oblicz(Operacja operacja, double liczbaA, double liczbaB)
        {
            double wynik = 0;

            if (operacja == Operacja.Dodawanie)
            {
                wynik = liczbaA + liczbaB;
            }
            else if (operacja == Operacja.Odejmowanie)
            {
                wynik = liczbaA - liczbaB;
            }
            else if (operacja == Operacja.Mnozenie)
            {
                wynik = liczbaA * liczbaB;
            }
            else if (operacja == Operacja.Dzielenie)
            {
                if (liczbaB != 0)
                {
                    wynik = liczbaA / liczbaB;
                }
                else
                {
                    throw new DivideByZeroException();
                }
            }
            else
            {
                throw new InvalidOperationException(INVALID_OPERATION_ERROR_MESSAGE.Translate());
            }

            return wynik;
        }
    }
}