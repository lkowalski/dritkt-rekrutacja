﻿using System;
using NUnit.Framework;
using Rekrutacja.Workers;
using Rekrutacja.Workers.Template;
using Soneta.Kadry;
using Soneta.Test;
using Soneta.Types;

namespace Rekrutacja.Tests
{
    [TestFixture]
    [TestDatabaseSrebrna]
    public class KalkulatorTests : DbTransactionTestBase
    {
        private const string FEATURE_OPERATION_DATE = "DataObliczen";
        private const string FEATURE_RESULT = "Wynik";

        private Pracownik Pracownik1 => Get<Pracownik>(new Guid("fa6e98aa-d7bf-4266-aec9-0e7214fb4fbf"));

        private Pracownik Pracownik2 => Get<Pracownik>(new Guid("47fb21dc-a45d-455d-a7be-2c68241975fd"));

        private Date _fakeDate = new Date(2021, 5, 1);

        [Test]
        public void KalkulatorWorker_UstawiaDateObliczen()
        {
            var worker = new KalkulatorWorker()
            {
                Params = CreateKalkulatorWorkerParams(1, 2, Operacja.Dodawanie, _fakeDate),
                Pracownicy = new[] { Pracownik1, Pracownik2 }
            };

            worker.WykonajAkcje();

            Assert.AreEqual(_fakeDate, Pracownik1.Features[FEATURE_OPERATION_DATE]);
            Assert.AreEqual(_fakeDate, Pracownik2.Features[FEATURE_OPERATION_DATE]);
        }

        [TestCase(Operacja.Dodawanie, 10, 20, ExpectedResult = 30)]
        [TestCase(Operacja.Odejmowanie, 10, 20, ExpectedResult = -10)]
        [TestCase(Operacja.Mnozenie, 10, 20, ExpectedResult = 200)]
        [TestCase(Operacja.Dzielenie, 10, 20, ExpectedResult = 0.5)]
        public double KalkulatorWorker_UstawiaPrawidlowyWynikObliczen(Operacja operacja, double liczbaA, double liczbaB)
        {
            var worker = new KalkulatorWorker()
            {
                Params = CreateKalkulatorWorkerParams(liczbaA, liczbaB, operacja, _fakeDate),
                Pracownicy = new[] { Pracownik1 }
            };

            worker.WykonajAkcje();

            return (double) Pracownik1.Features[FEATURE_RESULT];
        }

        [Test]
        public void KalkulatorWorker_DzieleniePrzezZero_ZglaszaWyjatek()
        {
            var worker = new KalkulatorWorker()
            {
                Params = CreateKalkulatorWorkerParams(10, 0, Operacja.Dzielenie, _fakeDate),
                Pracownicy = new[] { Pracownik1, Pracownik2 }
            };

            Assert.Throws<DivideByZeroException>(worker.WykonajAkcje);
        }

        private KalkulatorWorkerParams CreateKalkulatorWorkerParams(double liczbaA, double liczbaB, Operacja operacja,
            Date data)
        {
            return new KalkulatorWorkerParams(Context)
            {
                LiczbaA = liczbaA,
                LiczbaB = liczbaB,
                Operacja = operacja,
                DataObliczen = data,
            };
        }
    }
}
